from google.cloud import storage
from src.utils.logger import logging

LOG = logging.getLogger(__name__)

class Storage():

    def __init__(self,
                 project_id: str):
        self._project_id = project_id
        self.init_client_service()
        self._client = None
        self.init_client_service()

    def init_client_service(self):
        self._client = storage.Client(project=self.project_id)

    def upload_blob(self,
                    bucket_name,
                    source_file_name,
                    destination_file_name):
        LOG.info('Uploading blob gs://{}/{} from {}'.format(bucket_name,
                                                            destination_file_name,
                                                            source_file_name))
        bucket = self.client.bucket(bucket_name)
        blob = bucket.blob(destination_file_name)
        blob.upload_from_filename(source_file_name)

    @property
    def project_id(self):
        return self._project_id

    @property
    def client(self):
        return self._client
