from google.cloud import bigquery
from google.api_core.exceptions import Conflict
from src.utils.logger import logging

LOG = logging.getLogger(__name__)

class BigQuery():

    def __init__(self,
                 project_id: str):
        self._project_id = project_id

    def create_dataset(self,
                       dataset_id):
        client = bigquery.Client()
        dataset = bigquery.Dataset('{}.{}'.format(self.project_id, dataset_id))
        dataset.location = "US"
        try:
            LOG.info('Creating dataset {}'.format(dataset_id))
            dataset = client.create_dataset(dataset,
                                            timeout=30)
        except Conflict:
            LOG.info("Dataset {} already exists.".format(dataset_id))

    def create_schema(self,
                      json_schema):
        return [ bigquery.SchemaField(field,
                                      'STRING',
                                      'NULLABLE',
                                      'No data') \
            for field in json_schema ]
    
    def load(self,
             source_data,
             table_id,
             schema,
             delimiter):
        LOG.info('Loading {} on BigQuery from {}'.format(table_id, source_data))
        client = bigquery.Client()
        job_config = bigquery.LoadJobConfig(
            time_partitioning=bigquery.TimePartitioning(
                type_=bigquery.TimePartitioningType.DAY
            ),
            write_disposition=bigquery.WriteDisposition.WRITE_TRUNCATE
        )
        load_job = client.load_table_from_dataframe(source_data,
                                                    table_id,
                                                    job_config=job_config)
        load_job.result()
        destination_table = client.get_table(table_id)
        LOG.info("Loaded {} rows.".format(destination_table.num_rows))

    @property
    def project_id(self):
        return self._project_id
