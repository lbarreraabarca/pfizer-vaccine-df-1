import os
import sys
from src.models.AbstractRequest import AbstractRequest
from src.utils.logger import logging

abspath = os.path.abspath(__file__)
dirname = os.path.dirname(os.path.dirname(os.path.dirname(abspath)))
sys.path.append(os.path.join(dirname))

LOG = logging.getLogger(__name__)

class QualityRequest(AbstractRequest):

    def __init__(self,
                 project,
                 dataset,
                 table,
                 partitiondate,
                 bucket_raw):
        super().__init__()
        self._project = project
        self._dataset = dataset
        self._table = table
        self._partitiondate = partitiondate
        self._bucket_raw = bucket_raw
        
    def validate(self):
        if self.project is None or self.project == '':
            raise Exception('PROJECT_ID cannot be None or empty.')
        if self.dataset is None or self.dataset == '':
            raise Exception('DATASET cannot be None or empty.')
        if self.table is None or self.table == '':
            raise Exception('TABLE cannot be None or empty.')
        if self.partitiondate is None or self.partitiondate == '':
            raise Exception('PARTITIONDATE cannot be None or empty.')

    @classmethod
    def from_dict(cls, i_dict):
        return QualityRequest(project=i_dict['PROJECT_ID'].lower(),
                              dataset=i_dict['DATASET'].lower(),
                              table=i_dict['TABLE'].lower(),
                              partitiondate=i_dict['PARTITIONDATE'],
                              bucket_raw=i_dict['BUCKET_RAW'])

    @property
    def project(self):
        return self._project

    @property
    def dataset(self):
        return self._dataset

    @property
    def table(self):
        return self._table

    @property
    def partitiondate(self):
        return self._partitiondate

    @property
    def bucket_raw(self):
        return self._bucket_raw
