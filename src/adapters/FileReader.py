import os
import sys

abspath = os.path.abspath(__file__)
dirname = os.path.dirname(os.path.dirname(os.path.dirname(abspath)))
sys.path.append(os.path.join(dirname))


class FileReader():

    def load_file(self, file_path):
        self.validate(file_path)
        try:
            return open(file_path, "r").read()
        except Exception as ex:
            raise Exception(str(ex))

    def validate(self, file_path):
        if file_path is None:
            raise Exception('File Path cannot be null')
        if file_path == '':
            raise Exception('File Path cannot be empty')
        if not os.path.isfile(file_path):
            raise Exception('File does not Exist')
