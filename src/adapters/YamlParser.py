import os
import sys
import yaml

abspath = os.path.abspath(__file__)
dirname = os.path.dirname(os.path.dirname(os.path.dirname(abspath)))
sys.path.append(os.path.join(dirname))

class YamlParser():

    def parse(self, data):
        if data is None:
            raise Exception('Input YAML cannot be null')
        if data == '':
            raise Exception('Input YAML cannot be empty')
        try:
            return self.yaml_to_dict(data)
        except Exception as ex:
            raise Exception(str(ex))

    def yaml_to_dict(self, data):
        try:
            result = yaml.load(data, Loader=yaml.BaseLoader)
            return result
        except Exception:
            raise Exception('Input YAML is invalid')
