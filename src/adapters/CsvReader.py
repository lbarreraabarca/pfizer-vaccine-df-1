import uuid
import pandas as pd
from src.utils.logger import logging

LOG = logging.getLogger(__name__)

class CsvReader():

    def __init__(self,
                 filename: str,
                 columns: list):
        self._filename = filename
        self._columns = columns

    def read_file(self):
        return pd.read_csv(self.filename,
                           usecols=self.columns,
                           sep=',')

    @property
    def filename(self):
        return self._filename

    @property
    def columns(self):
        return self._columns
