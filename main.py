from src.utils.logger import logging
from src.adapters.CsvReader import CsvReader
from src.adapters.FileReader import FileReader
from src.adapters.YamlParser import YamlParser

from src.models.QualityRequest import QualityRequest
from src.services.BigQuery import BigQuery
from src.services.Storage import Storage

LOG = logging.getLogger(__name__)

if __name__ == '__main__':
    LOG.info("Reading request.yaml.")
    input_file = FileReader().load_file("request.yaml")
    LOG.info("Parsing to Yaml.")
    parsed_request = YamlParser().parse(input_file)

    LOG.info("Generating request Object.")
    request = QualityRequest.from_dict(parsed_request)

    ## Reading Dataframe vaccination_tweets.csv
    LOG.info("Reading CSV file {}.".format("vaccination_tweets.csv"))
    columns_csv = [
        "id",
        "user_name",
        "user_location",
        "user_description",
        "user_created",
        "user_followers",
        "user_verified",
        "date",
        "text",
        "hashtags",
        "source",
        "retweets",
        "favorites",
        "is_retweet"
    ]
    csv = CsvReader("vaccination_tweets.csv", columns_csv)
    dataframe = csv.read_file()

    ##BigQuery Operations
    bq = BigQuery(request.project)
    bq.create_dataset(request.dataset)
    schema = bq.create_schema(columns_csv)

    table_id = '{}.{}${}'.format(request.dataset,
                                 request.table,
                                 request.partitiondate.replace('-', ''))
    bq.load(dataframe, table_id, schema, ',')
