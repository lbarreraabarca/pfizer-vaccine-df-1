from airflow import models
from datetime import datetime, timedelta
from airflow import macros
from airflow.contrib.operators.gcs_to_sftp import GCSToSFTPOperator


GCP_CONNECTION_ID = 'google_cloud_default'
PROCESS_DATE = '{{ macros.ds_add(ds, -1).replace("-", "/") }}'

DAG_NAME = 'process_two'

default_args = {
    'owner': 'lu.barreraabarca',
    'depends_on_past': False,
    'start_date': datetime(2000, 1, 1),
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=2)
}

with models.DAG(
    DAG_NAME,
    catchup=False,
    default_args=default_args,
    schedule_interval='15 17 * * *') as dag:
    
    copy_file_sftp = GCSToSFTPOperator(
        task_id="copy_file_sftp",
        source_bucket='fala-pfizer-raw-data-0ff39af2',
        source_object='pzifer/ind_pzifer_tweets/{{ macros.ds_add(ds, -1).replace("-", "/") }}/pfizer_{{ macros.ds_add(ds, -1).replace("-", "") }}.avro',
        destination_path='35.192.152.235/var/www/gcs/pzifer/ind_pzifer_tweets/{{ macros.ds_add(ds, -1).replace("-", "/") }}/pfizer_{{ macros.ds_add(ds, -1).replace("-", "") }}.avro',
    )


