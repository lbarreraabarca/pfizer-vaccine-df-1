from airflow import models
from datetime import datetime, timedelta
from airflow import macros
from airflow.operators.bash_operator import BashOperator
from airflow.contrib.operators.bigquery_operator import BigQueryOperator
from airflow.contrib.operators.bigquery_to_gcs import BigQueryToCloudStorageOperator
from airflow.contrib.operators import gcs_to_bq


GCP_CONNECTION_ID = 'google_cloud_default'
PROCESS_DATE = '{{ macros.ds_add(ds, -1).replace("-", "/") }}'

DAG_NAME = 'process_one'

default_args = {
    'owner': 'lu.barreraabarca',
    'depends_on_past': False,
    'start_date': datetime(2000, 1, 1),
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=2)
}

query="""
SELECT id,
user_name,
user_location,
user_description,
user_created,
user_followers,
user_verified,
date,
text,
hashtags,
source,
retweets,
favorites,
is_retweet
FROM `fala-pfizer-vaccine-29f171ab.pzifer.ind_pzifer_tweets`
WHERE _PARTITIONDATE = CAST('2021-01-31' AS DATE)
AND user_verified is TRUE
""".format(PROCESS_DATE=PROCESS_DATE)

with models.DAG(
    DAG_NAME,
    catchup=False,
    default_args=default_args,
    schedule_interval='0 17 * * *') as dag:
    
    bq_query = BigQueryOperator(
        task_id='bq_query',
        sql=query,
        destination_dataset_table='fala-pfizer-vaccine-29f171ab.pzifer.trf_pfizer_verified_user',
        write_disposition='WRITE_TRUNCATE',
        allow_large_results=True,
        bigquery_conn_id='bigquery_default',
        use_legacy_sql=False,
        create_disposition='CREATE_IF_NEEDED',
        priority='INTERACTIVE',
        location='US'
    )
    
    bq_to_gcs = BigQueryToCloudStorageOperator(
        task_id='bq_to_gcs',
        source_project_dataset_table='pzifer.ind_pzifer_tweets',
        destination_cloud_storage_uris='gs://fala-pfizer-raw-data-0ff39af2/pzifer/ind_pzifer_tweets/{{ macros.ds_add(ds, -1).replace("-", "/") }}/pfizer_{{ macros.ds_add(ds, -1).replace("-", "") }}.avro',
        compression='NONE',
        export_format='AVRO',
        bigquery_conn_id='bigquery_default',
        delegate_to=None,
        labels=None
    )
