## PFIZER VACCINE DF

Maintainer Luis Barrera Abarca

### Project
 - Project name : [fala-pfizer-vaccine-29f171ab](https://console.cloud.google.com/home/dashboard?project=fala-pfizer-vaccine-29f171ab)
 
### Storage
 - gs://fala-pfizer-raw-data-0ff39af2

### BigQuery
 - Tables on BigQuery
   - pzifer.ind_pzifer_tweets
   - pzifer.trf_pfizer_enc
   - pzifer.trf_pfizer_verified_user

### Composer
 - [Airflow](https://v0d5d2cac699b0ea2p-tp.appspot.com/)
